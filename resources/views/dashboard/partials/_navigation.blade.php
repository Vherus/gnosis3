<ul id="generalTab" class="nav nav-tabs responsive" style="border-bottom: 0px !important;">
    <li class="{{ $crit or '' }}"><a href="{{ route('dashboard.critical') }}">Critical Tests</a></li>
    <li class="{{ $qsta or '' }}"><a href="{{ route('dashboard.queue') }}">Queue Stats</a></li>
    <li class="{{ $appl or '' }}"><a href="{{ route('dashboard.application') }}">Application</a></li>
    <li class="{{ $infr or '' }}"><a href="{{ route('dashboard.infrastructure') }}">Infrastructure</a></li>
    <li class="{{ $syst or '' }}"><a href="{{ route('dashboard.system') }}">System</a></li>
    <li class="{{ $wind or '' }}"><a href="{{ route('dashboard.windows') }}">Windows</a></li>
    <li class="{{ $othe or '' }}"><a href="{{ route('dashboard.other') }}">Other</a></li>
</ul>