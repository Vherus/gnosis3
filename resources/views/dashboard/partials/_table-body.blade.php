<tbody>
@foreach($results as $item)
    <tr>
        <td class="tv-dash">
            {{ $item->test_name }}
        </td>

        <td class="tv-dash" style="min-width: 53px;">
            {{ $item->logical_name }}
        </td>

        @for($i = 0; $i < count($entities); $i++)
            @include('dashboard.partials._bar-colour', ['data' => $item->$entities[$i]["nacs"]])
        @endfor

    </tr>
@endforeach
</tbody>