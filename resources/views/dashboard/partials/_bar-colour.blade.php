<?php
    $dataAry = explode('|||', $data);

    $status = '';
    $barColour = 'grey';

    if (array_key_exists(1, $dataAry)) { // Translate the epoch time into DateTime object so we can format it for human eyes
        $dt = new DateTime("@$dataAry[1]");
        //array_splice($dataAry, 1, 1);

        $dataAry[3] = str_replace('\\', '\\\\', substr($dataAry[3], 0, 260)); // Replace single slashes with double slashes to escape for directories in string
        $dataAry[3] = trim(preg_replace('/\s+/', ' ', $dataAry[3])); // Remove double spaces to prevent hover box missing bug

        if (isset($dataAry[3][259])) { // isset is a language construct (faster than calling function like strlen)
            $dataAry[3] .= '...';
        }

        $dataAry[4] = str_replace('|', '', $dataAry[4]); // Some hostnames have a | in them for some reason

        switch ($dataAry[0]) {
            case '0':
                $status = 'PASS';
                $barColour = 'success';
                break;
            case '1':
                $status = 'WARNING';
                $barColour = 'warning';
                break;
            case '2':
                $status = 'FAIL';
                $barColour = 'danger';
                break;
            case '4':
                $status = 'DANGER';
                $barColour = 'violet';
                break;
            case '5':
                $status = 'ALERT';
                $barColour = 'emergency';
                break;
        }
    }
?>

<td class="tv-bars">
    @if($status != '')
        <a onmouseover="nhpup.popup('<strong>{{ $status }}</strong><br><br><strong>TEST</strong><br>{{ $dataAry[2] }}<br><br><strong>INFORMATION</strong><br>{{ $dataAry[3] }}<br><br><strong>HOST</strong><br>{{ $dataAry[4] }}<br><br><strong>TEST ID</strong><br>{{ $dataAry[5] }}<br><br><strong>Last Run: {{ $dt->format('H:i - D d M Y') }}</strong>');" href="#">
            <div class="progress progress-sm mbn">
                <div role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-{{ $barColour }}">
                </div>
            </div>
        </a>
    @elseif($status == '')
        <div class="progress progress-sm mbn">
            <div role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-grey">
            </div>
        </div>
    @endif
</td>