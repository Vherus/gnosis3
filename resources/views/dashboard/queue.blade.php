@extends('master')

@section('title')
    Dashboard - Queue Stats
@stop

@section('breadcrumb')
    <li><a href="{{ route('dashboard') }}">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
    <li><a href="{{ route('dashboard.queue') }}">Queue Stats</a></li>
@stop

@section('content')
    <div id="tab-general">
        <div class="row mbl">
            @include('dashboard.partials._navigation', ['qsta' => 'active'])

            <div id="generalTabContent" class="tab-content responsive">
                <div id="critical-tab" class="tab-pane fade in active">

                    <div id="sum_box">
                        <table class="table">
                            @include('dashboard.partials._table-headers', ['category' => 'Queue Stats', 'entities' => $entities])

                            @include('dashboard.partials._table-body', ['results' => $results, 'entities' => $entities])
                        </table>
                    </div>
                </div>
            </div>
        </div>
@stop