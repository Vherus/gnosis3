@extends('master')

@section('title')
    Dashboard - Infrastructure
@stop

@section('breadcrumb')
    <li><a href="{{ route('dashboard') }}">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
    <li><a href="{{ route('dashboard.infrastructure') }}">Infrastructure</a></li>
@stop

@section('content')
    <div id="tab-general">
        <div class="row mbl">
            @include('dashboard.partials._navigation', ['infr' => 'active'])

            <div id="generalTabContent" class="tab-content responsive">
                <div id="critical-tab" class="tab-pane fade in active">

                    <div id="sum_box">
                        <table class="table">
                            @include('dashboard.partials._table-headers', ['category' => 'CAR', 'entities' => $entities])

                            @include('dashboard.partials._table-body', ['results' => $carResults, 'entities' => $entities])
                        </table>
                    </div>

                    <div id="sum_box">
                        <table class="table">
                            @include('dashboard.partials._table-headers', ['category' => 'CDS Connection', 'entities' => $entities])

                            @include('dashboard.partials._table-body', ['results' => $cdsResults, 'entities' => $entities])
                        </table>
                    </div>

                    <div id="sum_box">
                        <table class="table">
                            @include('dashboard.partials._table-headers', ['category' => 'Network Interfaces', 'entities' => $entities])

                            @include('dashboard.partials._table-body', ['results' => $netResults, 'entities' => $entities])
                        </table>
                    </div>

                    <div id="sum_box">
                        <table class="table">
                            @include('dashboard.partials._table-headers', ['category' => 'Oracle Database Health', 'entities' => $entities])

                            @include('dashboard.partials._table-body', ['results' => $oracleResults, 'entities' => $entities])
                        </table>
                    </div>
                </div>
            </div>
        </div>
@stop