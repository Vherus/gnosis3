@extends('master')

@section('title')
    Dashboard - System
@stop

@section('breadcrumb')
    <li><a href="{{ route('dashboard') }}">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
    <li><a href="{{ route('dashboard.system') }}">System</a></li>
@stop

@section('content')
    <div id="tab-general">
        <div class="row mbl">
            @include('dashboard.partials._navigation', ['syst' => 'active'])

            <div id="generalTabContent" class="tab-content responsive">
                <div id="critical-tab" class="tab-pane fade in active">

                    <div id="sum_box">
                        <table class="table">
                            @include('dashboard.partials._table-headers', ['category' => 'System Health', 'entities' => $entities])

                            @include('dashboard.partials._table-body', ['results' => $healthResults, 'entities' => $entities])
                        </table>
                    </div>

                    <div id="sum_box">
                        <table class="table">
                            @include('dashboard.partials._table-headers', ['category' => 'Tablespaces', 'entities' => $entities])

                            @include('dashboard.partials._table-body', ['results' => $tablespaceResults, 'entities' => $entities])
                        </table>
                    </div>

                    <div id="sum_box">
                        <table class="table">
                            @include('dashboard.partials._table-headers', ['category' => 'VNA', 'entities' => $entities])

                            @include('dashboard.partials._table-body', ['results' => $vnaResults, 'entities' => $entities])
                        </table>
                    </div>

                </div>
            </div>
        </div>
@stop