<?php
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Accenture GNOSIS</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--<meta http-equiv="refresh" content="300">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/icons/favicon.ico">

    {!! HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700') !!}
    {!! HTML::style('http://fonts.googleapis.com/css?family=Oswald:400,700,300') !!}
    {!! HTML::style('styles/jquery-ui-1.10.4.custom.min.css') !!}
    {!! HTML::style('styles/font-awesome.min.css') !!}
    {!! HTML::style('styles/bootstrap.min.css') !!}
    {!! HTML::style('styles/all.css') !!}
    {!! HTML::style('styles/main.css') !!}
    {!! HTML::style('styles/style-responsive.css') !!}
    {!! HTML::style('styles/pace.css') !!}
    {!! HTML::style('styles/jquery.news-ticker.css') !!}
    {!! HTML::style('styles/jquery.dropdown.css') !!}

    {!! HTML::script('js/jquery-1.11.2.min.js') !!}
    {!! HTML::script('js/jquery-ui.js') !!}
    {!! HTML::script('js/loading-msgs.js') !!}

    <script type="text/javascript">

        jQuery.noConflict();

        function disableButton()
        {
            document.getElementById('export-btn').setAttribute("class", "disabled");
            document.getElementById('export-btn').innerHTML = "Exporting...";
        }

        var isLoaded = false;
        setTimeout(function() {
            if (!isLoaded) {
                jQuery(".loading-text").html(randomMessage);
                jQuery(".loading-text").fadeIn("slow");
            }
        }, 4000);

        jQuery(window).load(function() {
            isLoaded = true;
            jQuery(".loader").fadeOut("slow");
            jQuery(".loading-text").fadeOut("slow");
            setTimeout(function() {
                jQuery(".loading-text").fadeOut("slow");
            }, 500);
        })
    </script>
</head>

<body>
<div class="loader"></div>
<center><div class="loading-text" id="loading-text"></div></center>
{!! HTML::script('js/cookiechoices.js') !!}
<script>
    document.addEventListener('DOMContentLoaded', function(event) {
        cookieChoices.showCookieConsentDialog('Gnosis uses cookies to improve user experience. Using Gnosis in any way declares your consent to the use of these cookies.',
                'close message');
    });
</script>
<div>

    <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>

    <div id="header-topbar-option-demo" class="page-header-topbar">
        <nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3" class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a id="logo" href="./" class="navbar-brand"><span class="fa fa-rocket"></span><img src="../public/img/gnosis-logo.png" style="height: 45px; margin-top: -13px; margin-left: -96px;"/><span style="display: none" class="logo-text-icon"></span></a></div>

            <div class="topbar-main"><a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>


                <!--                 <form id="topbar-search" action="" method="" class="hidden-sm hidden-xs"> -->
                <!--                     <div class="input-icon right text-white"><a href="#"><i class="fa fa-search"></i></a><input type="text" placeholder="Search..." class="form-control text-white"/></div> -->
                <!--                 </form> -->

                <ul class="nav navbar navbar-top-links mbn" style="float:left; padding-left: 15px;">

                    [trustdropdown]

                </ul>

                <ul class="nav navbar navbar-top-links mbn" style="float:left; padding-left: 15px;">

                    [trustdropdown]

                </ul>

                <ul class="nav navbar navbar-top-links navbar-right mbn">


                    [login]


                </ul>


            </div>
        </nav>
    </div>
</div>

<div id="wrapper">
    <!--BEGIN SIDEBAR MENU-->
    <nav id="sidebar" role="navigation" data-step="2"
         data-position="right" class="navbar-default navbar-static-side">
        <div class="sidebar-collapse menu-scroll">
            <ul id="side-menu" class="nav">

                <div class="clearfix"></div>

                <li>
                <a href="{{ route('dashboard') }}"><i class="fa fa-tachometer fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i><span class="menu-title">Dashboard</span></a>
                </li>

                <li>
                <a href="ingest-rate"><i class="fa fa-desktop fa-fw">
                        <div class="icon-bg bg-pink"></div>
                    </i><span class="menu-title">
                        Ingestion
                        </span></a>

                </li>

                <!-- <li> -->
                <!--                     <a href="ingest-rate"><i class="fa fa-bar-chart-o fa-fw"> -->
                <!--                         <div class="icon-bg bg-green"></div> -->
                <!--                     </i><span class="menu-title">Ingest Rate</span></a> -->
                <!--                     </li> -->

                <li>
                <a href="trust-map"><i class="fa fa-edit fa-fw">
                        <div class="icon-bg bg-violet"></div>
                    </i><span class="menu-title">Geographics</span></a>
                </li>

                <li>
                <a href="praxis-tool"><i class="fa fa-database fa-fw">
                        <div class="icon-bg bg-red"></div>
                    </i><span class="menu-title">Praxis</span></a>

                </li>

                <li>
                <a href="service-desk"><i class="fa fa-th-list fa-fw">
                        <div class="icon-bg bg-blue"></div>
                    </i><span class="menu-title">Service Desk</span></a>

                </li>

                <li>
                <a href="pushback-report"><i class="fa fa-file-o fa-fw">
                        <div class="icon-bg bg-blue"></div>
                    </i><span class="menu-title">Pushbacks</span></a>

                </li>

                <li>
                <a href="san"><i class="fa fa-sitemap fa-fw">
                        <div class="icon-bg bg-blue"></div>
                    </i><span class="menu-title">SAN Usage</span></a>

                </li>

                <!--                     <li><a href="#"><i class="fa fa-file-o fa-fw"> -->
                <!--                         <div class="icon-bg bg-yellow"></div> -->
                <!--                     </i><span class="menu-title">Link</span></a> -->

                <!--                     </li> -->
                <!--                     <li><a href="#"><i class="fa fa-gift fa-fw"> -->
                <!--                         <div class="icon-bg bg-grey"></div> -->
                <!--                     </i><span class="menu-title">Link</span></a> -->

                <!--                     </li> -->
                <!--                     <li><a href="#"><i class="fa fa-sitemap fa-fw"> -->
                <!--                         <div class="icon-bg bg-dark"></div> -->
                <!--                     </i><span class="menu-title">Link</span></a> -->

                <!--                     </li> -->
                <!--                     <li><a href="#"><i class="fa fa-envelope-o"> -->
                <!--                         <div class="icon-bg bg-primary"></div> -->
                <!--                     </i><span class="menu-title">Link</span></a> -->

                <!--                     </li> -->
                <!--                     <li><a href="#"><i class="fa fa-send-o fa-fw"> -->
                <!--                         <div class="icon-bg bg-orange"></div> -->
                <!--                     </i><span class="menu-title">Link</span></a> -->

                <!--                     </li> -->
                <!--                     <li><a href="#"><i class="fa fa-slack fa-fw"> -->
                <!--                         <div class="icon-bg bg-green"></div> -->
                <!--                     </i><span class="menu-title">Link</span></a></li> -->

            </ul>
        </div>
    </nav>
    <!--END SIDEBAR MENU-->

<div id="page-wrapper">

    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                @yield('title')
            </div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{ route('index') }}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            @yield('breadcrumb')
        </ol>
        <div class="clearfix">
        </div>
    </div>

    <div class="page-content">

        @yield('content')

        <?php
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $start), 4);
        echo 'Content loaded in '.$total_time.' seconds.';
        ?>

</div>
<!--END PAGE WRAPPER-->
</div>
</div>
{!! HTML::script('js/jquery-migrate-1.2.1.min.js') !!}
{!! HTML::script('js/bootstrap.min.js') !!}
{!! HTML::script('js/bootstrap-hover-dropdown.js') !!}
{!! HTML::script('js/html5shiv.js') !!}
{!! HTML::script('js/respond.min.js') !!}
{!! HTML::script('js/jquery.metisMenu.js') !!}
{!! HTML::script('js/icheck.min.js') !!}
{!! HTML::script('js/custom.min.js') !!}
{!! HTML::script('js/jquery.menu.js') !!}
{!! HTML::script('js/pace.min.js') !!}
{!! HTML::script('js/responsive-tabs.js') !!}
{!! HTML::script('js/main.js') !!}
{!! HTML::script('js/hover.js') !!}
</body>
</html>