<?php

/**
 * This file is part of the core PHP package for Gnosis3.
 *
 * Copyright (c) 2015 Accenture Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Gnosis3
 * @author Nathan King <nathaniel.king@accenture.com>
 * @copyright 2015 Accenture Ltd.
 * @version 3.0
 */

namespace App\Helpers;

use Symfony\Component\Routing\Exception\InvalidParameterException;

class ArrayHelper
{
    /**
     * Modify each element in the array to be completely upper or lower case
     *
     * @example
     * $nacs = ['RDE', 'RR8'];
     * ArrayHelper::formatCase($nacs); // All nacs to lower case
     * ArrayHelper::formatCase($nacs, 'upper'); // All nacs to upper case
     *
     * @param array $array The array, passed by reference, to modify
     * @param string $case Modify items in the array to be 'lower' or 'upper' case
     */
    public static function formatCase(array &$array, $case = 'lower')
    {
        foreach ($array as &$item) {
            array_walk($item, function(&$value) use ($case) {
                switch ($case) {
                    case 'lower':
                        $value = strtolower($value);
                        break;
                    case 'upper':
                        $value = strtoupper($value);
                        break;
                    default:
                        throw new InvalidParameterException('Expected case to be lower or upper');
                        break;
                }
            });
        }
    }
}