<?php

namespace App\Http\Controllers;

use App\DAL\Cache\RetrievesDashboard;
use App\DAL\Cache\RetrievesNacsCodes;
use App\DAL\Repositories\Dashboard\OtherRepository;
use App\Helpers\ArrayHelper;
use App\Http\Requests;

class DashboardOtherController extends Controller
{
    use RetrievesNacsCodes, RetrievesDashboard;

    /**
     * Other dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $results = $this->retrieveDashboard('dash-other-all', OtherRepository::class);
        $entities = $this->retrieveNacsCodes();
        ArrayHelper::formatCase($entities);

        return view('dashboard.other', compact('results', 'entities'));
    }
}
