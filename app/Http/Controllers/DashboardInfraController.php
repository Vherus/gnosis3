<?php

namespace App\Http\Controllers;

use App\DAL\Cache\RetrievesDashboard;
use App\DAL\Cache\RetrievesNacsCodes;
use App\DAL\Repositories\Dashboard;
use App\Helpers\ArrayHelper;
use App\Http\Requests;

class DashboardInfraController extends Controller
{
    use RetrievesNacsCodes, RetrievesDashboard;

    /**
     * Infrastructure dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $carResults = $this->retrieveDashboard('car-all', Dashboard\CARRepository::class);
        $cdsResults = $this->retrieveDashboard('cds-all', Dashboard\CDSConnectionRepository::class);
        $netResults = $this->retrieveDashboard('net-interfaces-all', Dashboard\NetworkInterfaceRepository::class);
        $oracleResults = $this->retrieveDashboard('oracle-all', Dashboard\OracleDbHealthRepository::class);
        $entities = $this->retrieveNacsCodes();
        ArrayHelper::formatCase($entities);

        return view('dashboard.infrastructure', compact('carResults', 'cdsResults', 'netResults', 'oracleResults', 'entities'));
    }
}
