<?php

namespace App\Http\Controllers;

use App\DAL\Cache\RetrievesDashboard;
use App\DAL\Cache\RetrievesNacsCodes;
use App\DAL\Repositories\Dashboard\QueueStatRepository;
use App\Helpers\ArrayHelper;
use App\Http\Requests;

class DashboardQueueController extends Controller
{
    use RetrievesNacsCodes, RetrievesDashboard;

    /**
     * Critical and Queue stats dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $results = $this->retrieveDashboard('queue-all', QueueStatRepository::class);
        $entities = $this->retrieveNacsCodes();
        ArrayHelper::formatCase($entities);

        return view('dashboard.queue', compact('results', 'nacsCodes', 'entities'));
    }
}
