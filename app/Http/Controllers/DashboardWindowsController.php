<?php

namespace App\Http\Controllers;

use App\DAL\Cache\RetrievesDashboard;
use App\DAL\Cache\RetrievesNacsCodes;
use App\DAL\Repositories\Dashboard;
use App\Helpers\ArrayHelper;
use App\Http\Requests;

class DashboardWindowsController extends Controller
{
    use RetrievesNacsCodes, RetrievesDashboard;

    /**
     * Windows dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $appResults = $this->retrieveDashboard('win-app-all', Dashboard\WindowsApplicationRepository::class);
        $configResults = $this->retrieveDashboard('win-config-all', Dashboard\WindowsConfigurationRepository::class);
        $healthResults = $this->retrieveDashboard('win-health-all', Dashboard\WindowsHealthRepository::class);
        $networkResults = $this->retrieveDashboard('win-net-all', Dashboard\WindowsNetworkRepository::class);
        $serviceResults = $this->retrieveDashboard('win-service-all', Dashboard\WindowsServiceRepository::class);
        $entities = $this->retrieveNacsCodes();
        ArrayHelper::formatCase($entities);

        return view('dashboard.windows', compact('appResults', 'configResults', 'healthResults', 'networkResults', 'serviceResults', 'entities'));
    }
}
