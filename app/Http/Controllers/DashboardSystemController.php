<?php

namespace App\Http\Controllers;

use App\DAL\Cache\RetrievesDashboard;
use App\DAL\Cache\RetrievesNacsCodes;
use App\DAL\Repositories\Dashboard;
use App\Helpers\ArrayHelper;
use App\Http\Requests;

class DashboardSystemController extends Controller
{
    use RetrievesNacsCodes, RetrievesDashboard;

    /**
     * System dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $healthResults = $this->retrieveDashboard('sys-health-all', Dashboard\SystemHealthRepository::class);
        $tablespaceResults = $this->retrieveDashboard('tablespace-all', Dashboard\TablespaceRepository::class);
        $vnaResults = $this->retrieveDashboard('vna-all', Dashboard\VNARepository::class);
        $entities = $this->retrieveNacsCodes();
        ArrayHelper::formatCase($entities);

        return view('dashboard.system', compact('healthResults', 'tablespaceResults', 'vnaResults', 'entities'));
    }
}