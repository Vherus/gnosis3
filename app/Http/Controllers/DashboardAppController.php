<?php

namespace App\Http\Controllers;

use App\DAL\Cache\RetrievesDashboard;
use App\DAL\Cache\RetrievesNacsCodes;
use App\DAL\Repositories\Dashboard;
use App\Helpers\ArrayHelper;
use App\Http\Requests;

class DashboardAppController extends Controller
{
    use RetrievesNacsCodes, RetrievesDashboard;

    /**
     * Application dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $configResults = $this->retrieveDashboard('app-config-all', Dashboard\ApplicationConfigurationRepository::class);
        $healthResults = $this->retrieveDashboard('app-health-all', Dashboard\ApplicationHealthRepository::class);
        $licenseResults = $this->retrieveDashboard('app-licenses-all', Dashboard\ApplicationLicenseRepository::class);
        $entities = $this->retrieveNacsCodes();
        ArrayHelper::formatCase($entities);

        return view('dashboard.application', compact('configResults', 'healthResults', 'licenseResults', 'entities'));
    }
}
