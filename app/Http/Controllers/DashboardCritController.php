<?php

namespace App\Http\Controllers;

use App\DAL\Cache\RetrievesDashboard;
use App\DAL\Cache\RetrievesNacsCodes;
use App\DAL\Repositories\Dashboard\CriticalTestRepository;
use App\Helpers\ArrayHelper;
use App\Http\Requests;

class DashboardCritController extends Controller
{
    use RetrievesNacsCodes, RetrievesDashboard;

    /**
     * Critical Tests dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $results = $this->retrieveDashboard('crit-all', CriticalTestRepository::class);
        $entities = $this->retrieveNacsCodes();
        ArrayHelper::formatCase($entities);

        return view('dashboard.critical', compact('results', 'entities'));
    }
}