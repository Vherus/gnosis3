<?php

//--- Begin Dashboard Routes ---\\

Route::get('', [
    'as'    => 'index',
    'uses'  => 'DashboardCritController@index'
]);

Route::get('dashboard', [
    'as'    => 'dashboard',
    'uses'  => 'DashboardCritController@index'
]);

Route::get('dashboard/critical', [
    'as'    => 'dashboard.critical',
    'uses'  => 'DashboardCritController@index'
]);

Route::get('dashboard/queue-stats', [
    'as'    => 'dashboard.queue',
    'uses'  => 'DashboardQueueController@index'
]);

Route::get('dashboard/application', [
    'as'    => 'dashboard.application',
    'uses'  => 'DashboardAppController@index'
]);

Route::get('dashboard/infrastructure', [
    'as'    => 'dashboard.infrastructure',
    'uses'  => 'DashboardInfraController@index'
]);

Route::get('dashboard/system', [
    'as'    => 'dashboard.system',
    'uses'  => 'DashboardSystemController@index'
]);

Route::get('dashboard/windows', [
    'as'    => 'dashboard.windows',
    'uses'  => 'DashboardWindowsController@index'
]);

Route::get('dashboard/other', [
    'as'    => 'dashboard.other',
    'uses'  => 'DashboardOtherController@index'
]);

//--- End Dashboard Routes ---\\