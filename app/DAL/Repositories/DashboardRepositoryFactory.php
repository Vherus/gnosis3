<?php

/**
 * This file is part of the core PHP package for Gnosis3.
 *
 * Copyright (c) 2015 Accenture Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Gnosis3
 * @author Nathan King <nathaniel.king@accenture.com>
 * @copyright 2015 Accenture Ltd.
 * @version 3.0
 */

namespace App\DAL\Repositories;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Prophecy\Exception\Doubler\ClassNotFoundException;

class DashboardRepositoryFactory
{
    /**
     * Either pass the actual string representation of the class (e.g. 'CriticalTestRepository') or use
     * the static class method (e.g. CriticalTestRepository::class) to return the string representation for use
     *
     * @param string $repository
     * @return Repository
     */
    public static function build($repository)
    {
        if (!strpos($repository, '\\')) {
            $repository = 'App\\DAL\\Repositories\\Dashboard\\' . $repository;
        }

        if (class_exists($repository)) {
            $repository = new $repository();

            if ($repository instanceof Repository) {
                return $repository;
            } else {
                throw new InvalidArgumentException('Invalid class name passed to dashboard repository factory. Must be of type Repository.');
            }
        } else {
            throw new ClassNotFoundException('Invalid repository name "'.$repository.'" provided to dashboard repository factory.', $repository);
        }
    }
}