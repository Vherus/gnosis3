<?php

/**
 * This file is part of the core PHP package for Gnosis3.
 *
 * Copyright (c) 2015 Accenture Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Gnosis3
 * @author Nathan King <nathaniel.king@accenture.com>
 * @copyright 2015 Accenture Ltd.
 * @version 3.0
 */

namespace App\DAL\Repositories\Dashboard;

use App\DAL\Models\Dashboard\ApplicationConfiguration;
use App\DAL\Repositories\Repository;

class ApplicationConfigurationRepository extends Repository
{
    /**
     * Return an instance of the model to work with
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    function model()
    {
        return new ApplicationConfiguration();
    }
}