<?php

/**
 * This file is part of the core PHP package for Gnosis3.
 *
 * Copyright (c) 2015 Accenture Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Gnosis3
 * @author Nathan King <nathaniel.king@accenture.com>
 * @copyright 2015 Accenture Ltd.
 * @version 3.0
 */

namespace App\DAL\Repositories;

interface RepositoryInterface
{
    /**
     * Fetch all results.
     * <br><br>
     * @param array $columns (Optional) An array of specific column names; defaults to ['*']
     * @examples $critRepo->all(); <i>// Returns with all columns</i>
     * @examples $critRepo->all(['test_id', 'test_name']); <i>// All results but only 2 columns</i>
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all($columns = ['*']);

    /**
     * Fetch all results where a column is a specific value. Change operator to >, <, >=, <= etc.
     * if you wish to perform anything other than = respectively.
     * <br><br>
     * @param string $column The column to perform the operation on
     * @param string $value The value that the column should be
     * @param string $operator (Optional) The operator to use; defaults to '='
     * @param array $columns (Optional) An array of specific column names; defaults to ['*']
     * @example $critRepo->where('test_id', 'ALL_CRIT');
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function where($column, $value, $operator = '=', $columns = ['*']);

    /**
     * Fetch all results, ordered by a specific column defaulting to descending order.
     * <br><br>
     * @param string $column The column to order by
     * @param string $direction (Optional) The direction to order the results by; defaults to 'desc'
     * @param array $columns (Optional) An array of specific column names; defaults to ['*']
     * @example $critRepo->orderBy('test_id');
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function orderBy($column, $direction = 'desc', $columns = ['*']);

    /**
     * Fetch all results where a specific column is a certain value, order by a specific column.
     * <br><br>
     * @param string $wColumn The column to filter by in the where clause
     * @param string $wValue The value of the column to filter by in the where clause
     * @param string $oColumn The column to order by
     * @param string $operator (Optional) The operator to use for the where clause; defaults to '='
     * @param string $direction (Optional) The direction to order the results by; defaults to 'desc'
     * @param array $columns (Optional) An array of specific column names; defaults to ['*']
     * @example $critRepo->orderedWhere('test_id', 'ALL_CRIT', 'test_name');
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function orderedWhere($wColumn, $wValue, $oColumn, $operator = '=', $direction = 'desc', $columns = ['*']);

    /**
     * Fetch all results where a key is within an array of values.
     * <br><br>
     * @param string $key The column name to use
     * @param array $values The array of values to use
     * @param array $columns (Optional) An array of specific column names; defaults to ['*']
     * @example $critRepo->whereIn('test_id', ['CRIT_ALL', 'CRIT_SOME']);
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function whereIn($key, array $values, $columns = ['*']);

    /**
     * Creates a new record in the <b>model</b>. No changes will be committed to the database until
     * you call the save() method.
     * <br><br>
     * @param array $attributes The key => value attributes to create the record with
     * @example $critRepo->create(['test_id' => 'NEW_TEST_ID', 'test_name' => 'My Test']);
     * @return static
     */
    public function create(array $attributes);

    /**
     * Find a record by ID.
     * <br><br>
     * @param string $id The record ID to get.
     * @param array $columns (Optional) An array of specific column names; defaults to ['*']
     * @example $critRepo->find(17);
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function find($id, $columns = ['*']);

    /**
     * Find the first record where an attribute is a specific value.
     * <br><br>
     * @param string $attribute The column name to use
     * @param string $value The value of the column
     * @param string $operator (Optional) The custom operator to use; defaults to '='
     * @param array $columns (Optional) An array of specific column names; defaults to ['*']
     * @example $critRepo->findBy('test_id', 'CRIT_ALL');
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findBy($attribute, $value, $operator = '=', $columns = ['*']);

    /**
     * Update an existing record in the <i>model</i>. No changes will be committed
     * to the database until you call the save() method.
     * <br><br>
     * @param string $id The ID of the record to update
     * @param array $attributes The key => value array to update
     * @example $critRepo->update(12, ['test_id' => 'UPDATED_ID']);
     * @return bool|int
     */
    public function update($id, array $attributes);

    /**
     * Delete a record from the <i>model</i>. No changes will be committed
     * to the database until you call the save() method.
     * <br><br>
     * @param string $id The record ID to delete
     * @example $critRepo->delete(28);
     * @return int
     */
    public function delete($id);

    /**
     * Save all model changes. This will commit any and all changes on the model to the database.
     * There is no "oops" method. Make sure you know what you're doing before you call this on anything.
     * <br><br>
     * @example $critRepo->save();
     * @return bool
     */
    public function save();
}