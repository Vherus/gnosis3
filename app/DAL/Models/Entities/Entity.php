<?php

namespace App\DAL\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    /**
     * The database table used by the model
     *
     * @var string
     */
    protected $table = 'entities';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'nacs',
        'description',
        'address_1',
        'address_2',
        'address_3',
        'address_4',
        'address_5',
        'postcode',
        'shortname',
        'exited',
        'contract',
        'ris_vendor'
    ];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = ['id'];
}