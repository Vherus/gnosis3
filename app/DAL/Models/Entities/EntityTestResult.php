<?php

namespace App\DAL\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class EntityTestResult extends Model
{
    protected $table = 'mv_latest_entity_test_results_r3';

    protected $hidden = ['id'];
}
