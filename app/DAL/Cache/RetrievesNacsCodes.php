<?php

/**
 * This file is part of the core PHP package for Gnosis3.
 *
 * Copyright (c) 2015 Accenture Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Gnosis3
 * @author Nathan King <nathaniel.king@accenture.com>
 * @copyright 2015 Accenture Ltd.
 * @version 3.0
 */

namespace App\DAL\Cache;

use App\DAL\Repositories\Entities\EntityRepository;
use Illuminate\Support\Facades\Redis;

trait RetrievesNacsCodes
{
    /**
     * Retrieve an ordered array of nacs codes from cache (or create cache and return if it doesn't exist)
     *
     * Use "redis" to cache the nacs codes for 5 minutes to help load balancing and page loading times.
     * Will pull from database and store in cache if not already cached. Cache happens server-side, so if
     * one user loads the nacs code that aren't loaded into cache then the server will put that data in memory
     * to be used as cache. This should dramatically reduce the amount of database transactions.
     *
     * The PHP redis laravel facade allows us to use any command that redis offers (redis can be used with a
     * variety of platforms/languages)
     *
     * @return array
     */
    protected function retrieveNacsCodes()
    {
        if (!Redis::exists('nacs-codes')) {
            $repository = new EntityRepository();
            Redis::set('entities', json_encode($repository->orderedWhere('exited', 'false', 'nacs', '=', 'asc', ['nacs'])));
            Redis::expire('nacs-codes', 300);
        }

        return json_decode(Redis::get('entities'), true);
    }
}