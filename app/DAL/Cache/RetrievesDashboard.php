<?php

/**
 * This file is part of the core PHP package for Gnosis3.
 *
 * Copyright (c) 2015 Accenture Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Gnosis3
 * @author Nathan King <nathaniel.king@accenture.com>
 * @copyright 2015 Accenture Ltd.
 * @version 3.0
 */

namespace App\DAL\Cache;

use App\DAL\Repositories\DashboardRepositoryFactory;
use Illuminate\Support\Facades\Redis;

trait RetrievesDashboard
{
    /**
     * Use "redis" to cache the dashboard data for 5 minutes to help load balancing and page loading times.
     * Will pull from database and store in cache if not already cached. Cache happens server-side, so if
     * one user loads a dashboard that isn't loaded into cache then the server will put that data in memory
     * to be used as cache. This should dramatically reduce the amount of database transactions.
     *
     * The PHP redis laravel facade allows us to use any command that redis offers (redis can be used with a
     * variety of platforms/languages)
     *
     * @param string $redisKey
     * @param string $repository
     *
     * @return array
     */
    protected function retrieveDashboard($redisKey, $repository)
    {
        if (!Redis::exists($redisKey)) {
            $r = DashboardRepositoryFactory::build($repository);
            Redis::set($redisKey, json_encode($r->all()));
            Redis::expire($redisKey, 300);
        }

        return json_decode(Redis::get($redisKey));
    }
}