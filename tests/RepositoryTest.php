<?php

/**
 * This file is part of the core PHP package for Gnosis3.
 *
 * Copyright (c) 2015 Accenture Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Gnosis3
 * @author Nathan King <nathaniel.king@accenture.com>
 * @copyright 2015 Accenture Ltd.
 * @version 3.0
 */

namespace Tests;

use App\DAL\Repositories\Dashboard\CriticalTestRepository;

class RepositoryTest extends \TestCase
{
    public function testAllReturnsData()
    {
        $repository = new CriticalTestRepository();
        $this->assertNotEmpty($repository->all());
    }

    public function testAllInstanceOfCollection()
    {
        $repository = new CriticalTestRepository();
        $this->assertInstanceOf('\\Illuminate\\Database\\Eloquent\\Collection', $repository->all());
    }
}