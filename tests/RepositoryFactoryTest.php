<?php

/**
 * This file is part of the core PHP package for Gnosis3.
 *
 * Copyright (c) 2015 Accenture Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package Gnosis3
 * @author Nathan King <nathaniel.king@accenture.com>
 * @copyright 2015 Accenture Ltd.
 * @version 3.0
 */

namespace Tests;

use App\DAL\Repositories\DashboardRepositoryFactory;

class RepositoryFactoryTest extends \TestCase
{
    public function testReturnsInstanceOfDashboardRepository()
    {
        $repository = DashboardRepositoryFactory::build('CriticalTestRepository');
        $this->assertInstanceOf('App\\DAL\\Repositories\\Repository', $repository);
    }
}